# Satellite Ground Station
The Spacecruft Satellite Ground station is a libre design for tracking
satellites.

It is intended for use with the Libre Space Foundation's SatNOGS network,
and is based on their freely available files.

This design will work for daily, but not continuous operation.
This design did ~8,500 observations in ~3 months for SatNOGS,
then the Yaesu Az rotator failed, due to over-running the system.

A more robust system, designed for 24/7/365 continuous operation,
see the Mighty Satellite Ground Station (MSGS) repository:

* https://spacecruft.org/spacecruft/Mighty-Satellite-Ground-Station


# Overview
Designed for artificial satellite tracking.

* Libre.
* Completely documented design listing all parts and sources
  from Antennas to Zipties.
* All commodity-off-the-shelf (COTS) parts generally available in
  North American markets.
* Requires no special tools or skills to assemble.
  Just needs two adjustable wrenches, a screwdriver, and wire cutters.
* VHF and UHF antennas on crossboom.
* Software defined radio (SDR).
* Az/El rotator optimized for satellite tracking.
* Relatively low cost, but parts selected for quick deployment not price
  (e.g. no Arduino hacking, no soldering, no drilling, etc.
   which could lower price).
* Indoor computer and controllers.
* 110V AC power (e.g. not a solar system).
* Quick and easy mast deployment.
* Non-permanent mount, which can be easily moved.
* All parts shipping via courier (UPS, FedEx), no freight required.
* Easily serviced, upgraded, modified.


# Bill of Materials
The core document is the Satellite Ground Station Bill of Materials (BoM).
It is a spreadsheet in Libre Office format.

* [Satellite Ground Station Bill of Materials](docs/satellite-ground-station-bom.ods)

![Satellite Ground Station Bill of Materials image](img/satellite-ground-station-bom.png)
*Screenshot of part of BoM, see full spreadsheet for details.*


# Diagram
Diagram made with formerly draw.io, now diagram.net with AppImage.

![Satellite Ground Station Diagram](img/satellite-ground-station-diagram.png)


# SatNOGS
*A global network of satellite ground stations, designed as an open source
participatory project.*

* SatNOGS Website --- https://satnogs.org/
* SatNOGS Network --- https://network.satnogs.org/

The base design comes from the Libre Space Foundation.
The present design adds review of other designs in the SatNOGS network
and feedback from the #SatNOGS Matrix channel.

Upstream docs:

* SatNOGS Introduction --- https://wiki.satnogs.org/Introduction
* SatNOGS Ground Station Build Overview --- https://wiki.satnogs.org/Build
* Ground Stations --- https://wiki.satnogs.org/Ground_Stations
* Raspberry Pi --- https://wiki.satnogs.org/Raspberry_Pi
* Antennas --- https://wiki.satnogs.org/Antennas
* Software Defined Radio (SDR) --- https://wiki.satnogs.org/Software_Defined_Radio
* Yaesu G-5500 Rotator docs --- https://wiki.satnogs.org/G-5500
* Papers --- https://wiki.satnogs.org/Academic_Papers


# TODO

* Rotator cable assemblies requires to cut off 6-pin connectors and
  strip leads. Crimping O connectors is better, but ideally is an easy plug.

* Yaesu GS-232B in SatNOGS client uses an old Debian Buster hamlib package,
  which requires a patch or it will stutter.

* See "Evaluate or Find" tab in BoM spreadsheet.


# Copyright
Unofficial project, not part of official Libre Space Foundation software or
website. Upstream sources under their respective copyrights.

License: CC By SA 4.0 International and/or GPLv3+ at your discretion.

*Copyright &copy; 2022, Jeff Moe.*
